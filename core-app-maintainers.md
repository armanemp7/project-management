# UBports Core Apps 

## Core apps and their maintainers

- [Calculator](https://gitlab.com/ubports/apps/calculator-app):
    - [Brian Douglass](https://gitlab.com/bhdouglass)
    - TG: @bhdouglass
    - M: @bhdouglass:matrix.org
- [Doc Viewer](https://gitlab.com/ubports/apps/docviewer-app):
    - [Chris Clime](https://gitlab.com/balcy)
    - TG: @balcy
    - M: @chris:ubports.chat
- [File Manager](https://gitlab.com/ubports/apps/filemanager-app):
    - [Bjarne Roß](https://gitlab.com/nfsprodriver)
    - TG: @nfsprodriver
    - M: @nfsprodriver:matrix.org
- [Gallery](https://gitlab.com/ubports/apps/gallery-app):
    - [Emanuele Sorce](https://gitlab.com/TronFortyTwo)
    - TG: @TronFortyTwo
    - M: @tron42:matrix.org
- [Notes](https://gitlab.com/ubports/apps/notes-app)
    - [David Farkas](https://gitlab.com/farkasdvd)
    - TG: @farkasdvd
    - M: @farkasdvd:matrix.org
- [OpenStore](https://gitlab.com/theopenstore/openstore-app):
    - [Brian Douglass](https://gitlab.com/bhdouglass)
    - TG: @bhdouglass
    - M: @bhdouglass:matrix.org
- [TELEports](https://gitlab.com/ubports/apps/teleports)
    - [Florian Leeber](https://gitlab.com/Flohack74)
    - TG: @Flohack
    - M: @flohack:ubports.chat
- [Terminal](https://gitlab.com/ubports/apps/terminal-app):
    - [Walter Garcia-Fontes](https://gitlab.com/wgarcia)
    - TG: @wagafo
- [UBports Welcome App](https://gitlab.com/ubports/apps/ubports-app):
    - [Jan Sprinz](https://gitlab.com/NeoTheThird)
    - TG: @neothethird
    - M: @NeoTheThird:matrix.org
- [Weather App](https://gitlab.com/ubports/apps/weather-app):
    - [Daniel Frost](https://gitlab.com/Danfro)
    - TG: @Danfro
    - M: @danfro:ubports.chat

## Core apps seeking maintainers

- [Calendar](https://gitlab.com/ubports/apps/calendar-app)
- [Camera](https://gitlab.com/ubports/apps/camera-app)
- [Clock](https://gitlab.com/ubports/apps/clock-app)
- [Music](https://gitlab.com/ubports/apps/music-app)

## Deb-based core apps and their maintainers

- [Contacts](https://github.com/ubports/address-book-app):
    - [Alberto Mardegan](https://github.com/mardy)
    - TG: @mardytardi
- [Dialer](https://github.com/ubports/dialer-app)
    - [Lionel Duboeuf](https://github.com/lduboeuf)
    - TG: @lduboeuf
    - M: ???
- [Messenger](https://github.com/ubports/messaging-app)
    - [Lionel Duboeuf](https://github.com/lduboeuf)
    - TG: @lduboeuf
    - M: ???
- [Morph Browser](https://github.com/ubports/morph-browser):
    - [Chris Clime](https://gitlab.com/balcy)
    - TG: @balcy
    - M: @chris:ubports.chat

## Deb-based core apps seeking maintainers


- [Media Player](https://github.com/ubports/mediaplayer-app)
- [Printing](https://github.com/ubports/ubuntu-printing-app)

## Deprecated

- [Sudoku](https://github.com/ubports/sudoku-app)
- [Telegram](https://github.com/ubports/telegram-app)
- [Web Browser](https://github.com/ubports/webbrowser-app)
